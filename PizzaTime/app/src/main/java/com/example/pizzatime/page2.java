package com.example.pizzatime;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class page2 extends AppCompatActivity implements View.OnClickListener {

    Button btnAdd11, btnAdd12, btnAdd13;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);


        btnAdd11 = (Button) findViewById(R.id.button_p1);
        btnAdd11.setOnClickListener(this);
        btnAdd12 = (Button) findViewById(R.id.button_p2);
        btnAdd12.setOnClickListener(this);
        btnAdd13 = (Button) findViewById(R.id.button_p3);
        btnAdd13.setOnClickListener(this);

        dbHelper = new DBHelper(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();

        TextView txt1 = findViewById(R.id.textView_h1);
        TextView txt2 = findViewById(R.id.textView_h2);
        TextView txt3 = findViewById(R.id.textView_h3);

        switch (v.getId()) {

            case R.id.button_p1:
                cv.put(DBHelper.KEY_NAME, txt1.getText().toString());
                cv.put(DBHelper.KEY_AMOUNT, 1);
                cv.put(DBHelper.KEY_PRICE, -15);

                db.insert(DBHelper.TABLE_ORDERS, null, cv);
                break;
            case R.id.button_p2:
                cv.put(DBHelper.KEY_NAME, txt2.getText().toString());
                cv.put(DBHelper.KEY_AMOUNT, 1);
                cv.put(DBHelper.KEY_PRICE, 1300);

                db.insert(DBHelper.TABLE_ORDERS, null, cv);
                break;
            case R.id.button_p3:
                cv.put(DBHelper.KEY_NAME, txt3.getText().toString());
                cv.put(DBHelper.KEY_AMOUNT, 1);
                cv.put(DBHelper.KEY_PRICE, 1800);

                db.insert(DBHelper.TABLE_ORDERS, null, cv);
                break;
        }

        dbHelper.close();
    }

    public void pagePizzaStart(View v) {
        Intent intent = new Intent(this, page1.class);
        startActivity(intent);
    }

    public void pageColaStart(View v) {
        Intent intent = new Intent(this, page2.class);
        startActivity(intent);
    }

    public void pageBoxStart(View v) {
        Intent intent = new Intent(this, page3.class);
        startActivity(intent);
    }

    public void pageNavStart(View v) {
        Intent intent = new Intent(this, page4.class);
        startActivity(intent);
    }
}